unit uCalc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls;

type
  TfrmCalculadora = class(TForm)
    um: TButton;
    pnlFundos: TPanel;
    dois: TButton;
    tres: TButton;
    quatro: TButton;
    cinco: TButton;
    seis: TButton;
    sete: TButton;
    oito: TButton;
    nove: TButton;
    btnLimpa: TButton;
    btnZero: TButton;
    btnSoma: TButton;
    Igual: TButton;
    mskTela: TMaskEdit;
    virgula: TButton;
    procedure btnSomaClick(Sender: TObject);
    procedure IgualClick(Sender: TObject);
    procedure btnLimpaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NumeroClick(Sender: TObject);
    procedure mskTelaKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
     FOperacao : Char;
     FTotal : Double;
     FUltimoNumero: Double;

     procedure Calcular;
     procedure Limpar;

  public
    { Public declarations }
  end;

var
  frmCalculadora: TfrmCalculadora;

implementation

{$R *.dfm}

procedure TfrmCalculadora.NumeroClick(Sender: TObject);
begin
  if FUltimoNumero = 0 then
     mskTela.Text := TButton(Sender).Caption
  else
     mskTela.Text := mskTela.Text + TButton(Sender).Caption;

     FUltimoNumero := StrToFloat(mskTela.Text);
 end;


procedure TfrmCalculadora.btnSomaClick(Sender: TObject);
begin
   FUltimoNumero := StrToFloat(mskTela.Text);
   Calcular;
   FOperacao := '+';
end;

procedure TfrmCalculadora.Calcular;
begin
   case FOperacao of
      '+' : FTotal := FTotal + StrToFloat(MskTela.Text);
   end;

   mskTela.Text := FloatToStr(FTotal);
   FUltimoNumero := 0;
end;

procedure TfrmCalculadora.IgualClick(Sender: TObject);
  begin
   Calcular;
  end;

procedure TfrmCalculadora.btnLimpaClick(Sender: TObject);
  begin
   Limpar;
  end;

procedure TfrmCalculadora.FormCreate(Sender: TObject);
  begin
    Limpar;
  end;

  procedure TFrmCalculadora.Limpar;
  begin
   FUltimoNumero := 0;
   mskTela.Text := '0';
   FOperacao := '+';
   FTotal := 0;
  end;

procedure TfrmCalculadora.mskTelaKeyPress(Sender: TObject; var Key: Char);
begin
 if not (Key in ['0'..'9' , ',' , #8]) then
 key := #0;
end;

procedure TfrmCalculadora.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  begin
  if key = VK_RETURN then
   Igual.Click;

  if key = VK_ADD then
   btnSoma.Click;

  if key = VK_NUMPAD0 then
   btnZero.Click;

  if key = VK_NUMPAD1 then
   um.Click;

  if key = VK_NUMPAD2 then
   dois.Click;

  if key = VK_NUMPAD3 then
   tres.Click;

  if key = VK_NUMPAD4 then
   quatro.Click;

  if key = VK_NUMPAD5 then
   cinco.Click;

  if key = VK_NUMPAD6 then
   seis.Click;

  if key = VK_NUMPAD7 then
   sete.Click;

  if key = VK_NUMPAD8 then
   oito.Click;

  if key = VK_NUMPAD9 then
   nove.Click;

  if Key = VK_DECIMAL then
   virgula.Click;

  if Key = VK_DELETE then
    btnLimpa.click;

  end;
end.
