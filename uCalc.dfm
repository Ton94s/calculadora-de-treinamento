object frmCalculadora: TfrmCalculadora
  Left = 1650
  Top = 230
  ActiveControl = mskTela
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Calculadora'
  ClientHeight = 505
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object pnlFundos: TPanel
    Left = 0
    Top = 0
    Width = 500
    Height = 505
    Align = alClient
    TabOrder = 1
  end
  object um: TButton
    Left = 8
    Top = 312
    Width = 113
    Height = 89
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = NumeroClick
  end
  object dois: TButton
    Left = 136
    Top = 312
    Width = 113
    Height = 89
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = NumeroClick
  end
  object tres: TButton
    Left = 265
    Top = 312
    Width = 113
    Height = 89
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = NumeroClick
  end
  object quatro: TButton
    Left = 8
    Top = 210
    Width = 113
    Height = 89
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = NumeroClick
  end
  object cinco: TButton
    Left = 136
    Top = 208
    Width = 113
    Height = 89
    Caption = '5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = NumeroClick
  end
  object seis: TButton
    Left = 265
    Top = 209
    Width = 113
    Height = 89
    Caption = '6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = NumeroClick
  end
  object sete: TButton
    Left = 8
    Top = 113
    Width = 113
    Height = 89
    Caption = '7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = NumeroClick
  end
  object oito: TButton
    Left = 136
    Top = 112
    Width = 113
    Height = 89
    Caption = '8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = NumeroClick
  end
  object nove: TButton
    Left = 264
    Top = 112
    Width = 113
    Height = 89
    Caption = '9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = NumeroClick
  end
  object btnLimpa: TButton
    Left = 8
    Top = 408
    Width = 113
    Height = 89
    Caption = 'C'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = btnLimpaClick
  end
  object btnZero: TButton
    Left = 137
    Top = 407
    Width = 112
    Height = 89
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    OnClick = NumeroClick
  end
  object btnSoma: TButton
    Left = 384
    Top = 113
    Width = 113
    Height = 193
    Caption = '+'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = btnSomaClick
  end
  object Igual: TButton
    Left = 384
    Top = 312
    Width = 113
    Height = 185
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    OnClick = IgualClick
  end
  object mskTela: TMaskEdit
    Left = 0
    Top = 8
    Width = 495
    Height = 80
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -64
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 14
    OnKeyPress = mskTelaKeyPress
  end
  object virgula: TButton
    Left = 265
    Top = 407
    Width = 112
    Height = 89
    Caption = ','
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
    OnClick = NumeroClick
  end
end
